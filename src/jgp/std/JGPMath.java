package jgp.std;

import java.io.UnsupportedEncodingException;

import javax.swing.JOptionPane;

public class JGPMath {
    public static short little2big(short value) {
    	int b1 = value & 0xff;
    	int b2 = (value >> 8) & 0xff;

    	return (short) (b1 << 8 | b2 << 0);
    }
    public static int little2big(int i) {
        return (i&0xff)<<24 | (i&0xff00)<<8 | (i&0xff0000)>>8 | (i>>24)&0xff;
    }
    public static int stringByteLength(String str) {
    	if(str == null)
    		return 0;
    	else if(str.isEmpty())
    		return 2;
    	else {
    		byte ch[] = null;
    		try {
    			ch = str.getBytes("UTF-8");
    		} catch (UnsupportedEncodingException ex) {
    			JOptionPane.showMessageDialog(null, "B��d biblioteki jgp.std.JGPMath - stringByteLength, nieprawid�owa obs�uga stringu UTF-8.", "Error", JOptionPane.ERROR_MESSAGE);
    		}
    		return ch.length + 2; // Metoda zwraca liczb� bajt�w stringu plus 2, kt�re okre�laj� jego d�ugo��. Jest to ca�kowita liczba bajt�w zajmowana przez string.
    	}
    }
}
