package jgp.std;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class JGPIO {
	public static boolean equalDirectory(File dir1, File dir2) throws IOException {
		if(!dir1.isDirectory() || !dir2.isDirectory())
			return false;
		
		File files1[] = dir1.listFiles();
		File files2[] = dir2.listFiles();
		
		if(files1.length != files2.length)
			return false;
		
		for(int i = 0; i < files1.length; i++)
			if(files1[i].getName().compareTo(files2[i].getName()) != 0)
				return false;
		return true;
	}
	public static boolean equalFile(File file1, File file2) throws IOException {
		if(!file1.isFile() || !file2.isFile())
			return false;
		boolean equal = true;
	    FileInputStream fi1 = new FileInputStream(file1);      
	    FileInputStream fi2 = new FileInputStream(file2);
	    
	    if(file1.length() != file2.length())
	    	equal = false;
	    else {
	    	for(int i = 0; i < file1.length(); i++) {
	    		if(fi1.read() != fi2.read()) {
	    	    	equal = false;
	    	    	break;
	    		}
	    	}
	    }
    	fi1.close();
    	fi2.close();
	    return equal;
	}
}
